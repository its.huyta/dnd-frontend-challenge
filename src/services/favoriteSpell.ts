import { getFavoriteSpellsFromDB, saveFavoriteSpellsToDB } from '@/persistence/db/favorite';
import { Spell } from '@/types/spell';

export class FavoriteSpellService {
  static async addFavoriteSpell(spell: Spell) {
    const spells = await this.getFavoriteSpells();

    saveFavoriteSpellsToDB([...spells, spell]);
  }

  static async removeFavoriteSpell(spell: Spell) {
    const favoriteSpells = await this.getFavoriteSpells();

    saveFavoriteSpellsToDB(favoriteSpells.filter((favoriteSpell) => favoriteSpell.index !== spell.index));
  }

  static async getFavoriteSpells() {
    return getFavoriteSpellsFromDB();
  }
}
