import { ClassIndex } from '@/types';

export const getClassIconUrl = (classIndex: ClassIndex | string) => {
  return `/classes/${classIndex.toLowerCase()}.png`;
};
