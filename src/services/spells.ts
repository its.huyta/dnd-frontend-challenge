import { paramCase } from 'change-case';

import { SPELL_ICON_ORIGIN_URL } from '@/config';
import { ClassIndex } from '@/types';
import { joinURL } from '@/utils/url';

export const spellIconNameExceptions: Record<string, string | undefined> = {
  'Animate Dead': 'necromancy',
  'Animate Objects': 'animate-object',
  'Animal Shapes': 'animal-friendship',
};

export const getSpellIconUrl = (spellName: string) => {
  const imageName = spellIconNameExceptions[spellName] || spellName;

  return joinURL(SPELL_ICON_ORIGIN_URL, `${paramCase(imageName)}.png`);
};

export const getFallbackSpellIconUrl = () => {
  return joinURL(SPELL_ICON_ORIGIN_URL, 'conjuration.png');
};

export const allClasses = Object.values(ClassIndex);
