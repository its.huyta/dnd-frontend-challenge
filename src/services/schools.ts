import { School } from '@/types';

export const getSchoolIconUrl = (school?: School) => {
  return `/schools/${school?.toLowerCase()}.png`;
};
