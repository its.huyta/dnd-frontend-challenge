import { createBrowserRouter } from 'react-router-dom';

import Layout from '@/components/Layout';
import FavoriteSpellsPage from '@/pages/FavoriteSpellsPage/FavoriteSpellsPage';
import SpellListPage from '@/pages/SpellListPage';

export const router = createBrowserRouter([
  {
    element: <Layout />,
    children: [
      {
        path: '/',
        element: <SpellListPage />,
      },
      {
        path: '/favorite',
        element: <FavoriteSpellsPage />,
      },
    ],
  },
]);
