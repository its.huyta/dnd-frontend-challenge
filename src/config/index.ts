// ========= Endpoints ===========
export const REST_API_ORIGIN_URL = 'https://www.dnd5eapi.co/api/';
export const GRAPHQL_API_ORIGIN_URL = 'https://www.dnd5eapi.co/graphql/';
export const SPELL_ICON_ORIGIN_URL = 'https://mythical.ink/img/icons/dnd5e/';

// ========== Default settings =========
export const DEFAULT_OFFSET = 0;
export const DEFAULT_LIMIT = 10;

// ========== Database =========
export const FAVORITE_SPELLS_DB_KEY = 'favorite_spells';
