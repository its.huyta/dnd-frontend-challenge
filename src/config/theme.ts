import brown from '@mui/material/colors/brown';
import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      ...brown,
      main: '#d0a85d',
    },
    secondary: {
      main: '#111111',
    },
  },
});

export default theme;
