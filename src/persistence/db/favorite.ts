import { FAVORITE_SPELLS_DB_KEY } from '@/config';
import { getFromDB, saveToDB } from '@/persistence/db/core';
import { Spell } from '@/types/spell';

export const saveFavoriteSpellsToDB = (favoriteSpells: Spell[]) => {
  return saveToDB(FAVORITE_SPELLS_DB_KEY, JSON.stringify(favoriteSpells));
};

export const getFavoriteSpellsFromDB = async () => {
  try {
    const rawFavoriteSpells = await getFromDB(FAVORITE_SPELLS_DB_KEY);

    if (!rawFavoriteSpells) return [];

    const parsedFavoriteSpells = JSON.parse(rawFavoriteSpells);

    if (Array.isArray(parsedFavoriteSpells) && parsedFavoriteSpells.length > 0) return parsedFavoriteSpells as Spell[];

    return [];
  } catch (error) {
    console.error(error);

    return [];
  }
};
