import { gql } from '@apollo/client';

import { REST_API_ORIGIN_URL } from '@/config';
import { ClassIndex, RawSpell, Spell } from '@/types';
import { joinURL } from '@/utils/url';

export interface FetchSpellsResponse {
  spells: RawSpell[];
}

export interface FetchSpellsOptions {
  limit?: number;
  offset?: number;
  classIndex?: ClassIndex | string;
}

export const FETCH_SPELLS_QUERY = gql`
  query fetchSpellsQuery($limit: Int!, $offset: Int!, $classIndex: StringFilter) {
    spells(limit: $limit, skip: $offset, class: $classIndex) {
      index
      level
      name
      desc
      casting_time
      duration
      attack_type
      range
      school {
        name
        index
      }
      classes {
        name
      }
      damage {
        damage_type {
          name
        }
      }
    }
  }
`;

export const FETCH_SPELL_QUERY = gql`
  query fetchSpellQuery($index: String!) {
    spell(index: $index) {
      index
      level
      name
      desc
      casting_time
      duration
      attack_type
      range
      school {
        name
        index
      }
      classes {
        name
      }
      damage {
        damage_type {
          name
        }
      }
    }
  }
`;

export const fetchSpellCount = async (classIndex?: ClassIndex | string): Promise<number> => {
  const fetchURL = joinURL(REST_API_ORIGIN_URL, !classIndex ? '/api/spells' : `/api/classes/${classIndex}/spells`);

  const response = await fetch(fetchURL);
  const { count } = await response.json();

  return count;
};

export const normalizeSpell = (rawSpell: RawSpell): Spell => {
  return {
    index: rawSpell.index,
    level: rawSpell.level,
    name: rawSpell.name,
    descriptions: rawSpell.desc,
    castingTime: rawSpell.casting_time,
    duration: rawSpell.duration,
    attackType: rawSpell.attack_type,
    range: rawSpell.range,
    school: rawSpell.school.name,
    classes: rawSpell.classes,
    ...(rawSpell.damage?.damage_type?.name
      ? {
          damage: {
            type: rawSpell.damage.damage_type.name,
          },
        }
      : {}),
  };
};
