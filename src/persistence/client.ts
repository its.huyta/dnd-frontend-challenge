import { ApolloClient, InMemoryCache } from '@apollo/client';

import { GRAPHQL_API_ORIGIN_URL } from '@/config';

export const client = new ApolloClient({
  uri: GRAPHQL_API_ORIGIN_URL,
  cache: new InMemoryCache(),
});
