import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { memo } from 'react';

import EmptyPlaceholder from '@/components/EmptyPlaceholder';
import LogoWatermark from '@/components/LogoWatermark';
import SpellList from '@/components/SpellList';
import { useFavoriteSpells } from '@/hooks/useFavoriteSpells';
import DiamondDice from '@/icons/DiamondDice';

const FavoriteSpellsPage = () => {
  const { favoriteSpells, isFavoriteSpell, removeFavoriteSpell } = useFavoriteSpells();

  return (
    <Box padding={2}>
      <Box display="flex" justifyContent="center" alignItems="center" gap={1} mb={2}>
        <DiamondDice sx={{ fontSize: 48 }} />
        <Typography variant="h3" fontFamily="Amatic SC" fontWeight={700}>
          Favorite Spells
        </Typography>
      </Box>
      {!favoriteSpells.length ? (
        <Box display="flex" alignItems="center" justifyContent="center">
          <EmptyPlaceholder subtitleText="Try marking a spell as favorite." />
        </Box>
      ) : (
        <>
          <SpellList spells={favoriteSpells} isFavoriteSpell={isFavoriteSpell} onRemoveFavorite={removeFavoriteSpell} />
          <LogoWatermark />
        </>
      )}
    </Box>
  );
};

export default memo(FavoriteSpellsPage);
