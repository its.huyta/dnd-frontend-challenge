import Box from '@mui/material/Box';
import Pagination from '@mui/material/Pagination';
import { memo } from 'react';
import { createSearchParams, useNavigate, useSearchParams } from 'react-router-dom';

import EmptyPlaceholder from '@/components/EmptyPlaceholder';
import ErrorPlaceholder from '@/components/ErrorPlaceholder';
import LoadingState from '@/components/LoadingState';
import LogoWatermark from '@/components/LogoWatermark';
import SpellList from '@/components/SpellList';
import SpellFilters from '@/components/SpellList/SpellFilters';
import { DEFAULT_LIMIT } from '@/config';
import { useFavoriteSpells } from '@/hooks/useFavoriteSpells';
import { usePrevious } from '@/hooks/usePrevious';
import { useSpells } from '@/hooks/useSpells';
import { ClassIndex } from '@/types';

interface SpellListPageProps {
  children?: React.ReactNode;
}

const ITEMS_PER_PAGE = DEFAULT_LIMIT;

const createSearchForNavigate = ({ page, classIndex }: { page?: number; classIndex?: ClassIndex | string }) => {
  return `?${createSearchParams({
    ...(page ? { page: String(page) } : {}),
    ...(classIndex ? { classIndex } : {}),
  })}`;
};

const SpellListPage: React.FC<SpellListPageProps> = () => {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const page = Number(searchParams.get('page') || 1);
  const classIndex = searchParams.get('classIndex') || '';
  const offset = (page - 1) * ITEMS_PER_PAGE;
  const { loading, error, data, count } = useSpells({ offset, limit: DEFAULT_LIMIT, classIndex });
  const previousData = usePrevious(data);
  const previousCount = usePrevious(count);
  const { isFavoriteSpell, removeFavoriteSpell, addFavoriteSpell } = useFavoriteSpells();

  const handleChangePagination = (event: React.ChangeEvent<unknown>, value: number) => {
    navigate({
      pathname: '/',
      search: createSearchForNavigate({ page: value, classIndex }),
    });
  };

  const handleFilterClass = (classIndex: ClassIndex | string) => {
    navigate({
      pathname: '/',
      search: createSearchForNavigate({
        page: 1,
        classIndex,
      }),
    });
  };

  const dataToDisplay = data || previousData;
  const countToDisplay = count || previousCount;

  return (
    <Box padding={2}>
      {loading && <LoadingState />}
      <SpellFilters currentClassFilter={classIndex} onFilterClass={handleFilterClass} />
      {dataToDisplay && dataToDisplay.length > 0 && (
        <>
          <SpellList
            spells={dataToDisplay}
            isFavoriteSpell={isFavoriteSpell}
            onAddFavorite={addFavoriteSpell}
            onRemoveFavorite={removeFavoriteSpell}
          />
          <LogoWatermark />
        </>
      )}
      {!count && !loading ? (
        <Box display="flex" alignItems="center" justifyContent="center">
          <EmptyPlaceholder />
        </Box>
      ) : (
        <>
          {error && (
            <Box display="flex" alignItems="center" justifyContent="center">
              <ErrorPlaceholder />
            </Box>
          )}
          {!error && countToDisplay && count && (
            <Pagination
              count={Math.ceil(countToDisplay / ITEMS_PER_PAGE)}
              page={page}
              onChange={handleChangePagination}
              sx={{ mt: 2, display: 'flex', justifyContent: 'center' }}
            />
          )}
        </>
      )}
    </Box>
  );
};

export default memo(SpellListPage);
