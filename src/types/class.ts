export interface CharacterClass {
  name: string;
}

export enum ClassIndex {
  FIGHTER = 'fighter',
  BARD = 'bard',
  CLERIC = 'cleric',
  DRUID = 'druid',
  PALADIN = 'paladin',
  RANGER = 'ranger',
  SORCERER = 'sorcerer',
  WARLOCK = 'warlock',
  WIZARD = 'wizard',
}
