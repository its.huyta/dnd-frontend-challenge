import { CharacterClass } from './class';
import { School } from './school';

export interface RawSpell {
  index: string;
  level: number;
  name: string;
  desc: string[];
  casting_time: string;
  duration: string;
  range: string;
  school: {
    name: School;
  };
  classes: CharacterClass[];
  attack_type?: string;
  damage?: {
    damage_type: {
      name: string;
    };
  };
}

export interface Spell {
  index: string;
  level: number;
  name: string;
  descriptions: string[];
  castingTime: string;
  duration: string;
  range: string;
  school: School;
  attackType?: string;
  classes: CharacterClass[];
  damage?: {
    type: string;
  };
}
