import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon';

const Logo: React.FC<SvgIconProps> = ({ width = 200, height = 200, ...rest }) => {
  return (
    <SvgIcon width={width} height={height} viewBox="0 0 100 100" {...rest}>
      <path d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#c39031" stroke="none">
        <animateTransform
          attributeName="transform"
          type="rotate"
          dur="1s"
          repeatCount="indefinite"
          keyTimes="0;1"
          values="0 50 51;360 50 51"
        />
      </path>
    </SvgIcon>
  );
};

export default Logo;
