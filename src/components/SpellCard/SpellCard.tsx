import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import { alpha } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { upperCaseFirst } from 'upper-case-first';

import { Spell, truthy } from '@/types';

import LabeledValue from '../LabeledValue';
import SchoolIcon from '../SchoolIcon';
import SpellExtraDetails from '../SpellExtraDetails';
import SpellIcon from '../SpellIcon';
import ToggleFavoriteButton from '../ToggleFavoriteButton';

export interface SpellCardProps {
  spell: Spell;
  showFull?: boolean;
  isFavorite?: boolean;
  onAddFavorite?: (spell: Spell) => void;
  onRemoveFavorite?: (spell: Spell) => void;
}

export const getSpellLabeledValues = (spell: Spell) => {
  return [
    {
      label: 'Casting time',
      value: spell.castingTime,
    },
    {
      label: 'Duration',
      value: spell.duration,
    },
    {
      label: 'Range',
      value: spell.range,
    },
    spell.attackType && {
      label: 'Attack',
      value: spell.attackType,
    },
    spell.damage && {
      label: 'Damage',
      value: spell.damage.type,
    },
  ].filter(truthy);
};

const SpellCard: React.FC<SpellCardProps> = ({ spell, isFavorite = false, onAddFavorite, onRemoveFavorite }) => {
  const [shouldShowFull, setShouldShowFull] = useState(false);
  const spellLabeledValues = getSpellLabeledValues(spell);

  const handleToggleShowFull = () => {
    setShouldShowFull((showFull) => !showFull);
  };

  useEffect(() => {
    console.log(123, spell);
  }, []);

  return (
    <Card
      elevation={0}
      sx={{
        height: '100%',
        background: 'transparent',
        border: '1px solid #ababab',
        borderRadius: 'unset',
        cursor: 'pointer',
        '&:hover': {
          backgroundColor: (theme) => alpha(theme.palette.secondary.main, 0.05),
        },
      }}
      id={spell.index}
      onClick={handleToggleShowFull}
      data-testid={`spell-item-${spell.index}`}
    >
      <CardContent>
        <Box display="flex" justifyContent="space-between" gap={1} marginBottom={2}>
          <Box display="flex" alignItems="center" gap={1}>
            <SpellIcon spell={spell} />
            <Typography
              variant="h6"
              fontWeight={600}
              data-testid={`spell-name-${spell.index}`}
              sx={{ whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden' }}
            >
              {spell.name}
            </Typography>
            <Typography variant="body1" sx={{ whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden' }}>
              Lvl {spell.level}
            </Typography>
          </Box>
          <Box sx={{ display: 'flex', alignItems: 'center', gap: 1 }}>
            <SchoolIcon school={spell.school} />
            <ToggleFavoriteButton
              spell={spell}
              isFavorite={isFavorite}
              onAddFavorite={onAddFavorite}
              onRemoveFavorite={onRemoveFavorite}
            />
          </Box>
        </Box>
        <Grid container justifyContent="flex-start" gap={1}>
          {spellLabeledValues.map(({ label, value }) => (
            <Grid
              key={label}
              item
              width="100%"
              maxWidth={(theme) => ({
                xs: `calc(50% - ${theme.spacing(1)})`,
              })}
              lg={2}
            >
              <LabeledValue label={label} value={upperCaseFirst(value)} />
            </Grid>
          ))}
        </Grid>
        {shouldShowFull && (
          <Box className="animate__animated animate__fadeInDown">
            <Divider sx={{ borderColor: 'primary.main', borderWidth: 0.5, marginY: 2 }} />
            <SpellExtraDetails spell={spell} />
          </Box>
        )}
      </CardContent>
    </Card>
  );
};

export default SpellCard;
