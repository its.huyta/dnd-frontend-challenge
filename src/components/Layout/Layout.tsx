import Box from '@mui/material/Box';
import { Outlet } from 'react-router-dom';

import AppBar from '@/components/AppBar';

const Layout = () => {
  return (
    <Box display="flex" flexDirection="column" height="100%" flexGrow={1}>
      <AppBar />
      <Outlet />
    </Box>
  );
};

export default Layout;
