import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

export interface LabeledValueProps {
  label?: string;
  value?: string | number;
}

const LabeledValue: React.FC<LabeledValueProps> = ({ label, value }) => {
  return (
    <Box display="flex" flexDirection="column">
      <Typography variant="body2" fontWeight={700} textTransform="uppercase" color="primary.dark">
        {label}
      </Typography>
      <Typography variant="body1">{value}</Typography>
    </Box>
  );
};

export default LabeledValue;
