import MuiAppBar from '@mui/material/AppBar';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';

import DiamondDice from '@/icons/DiamondDice';
import Logo from '@/icons/Logo';

const StyledLink = styled(Link)(() => ({
  color: '#eb0029',
  textDecoration: 'none',
  '&:hover': {
    '.MuiSvgIcon-root': {
      fontSize: 56,
      transition: 'all 0.2s',
    },
  },
}));

const AppBar = () => {
  return (
    <MuiAppBar position="sticky" color="secondary">
      <Container maxWidth="xl">
        <Toolbar disableGutters sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <StyledLink to="/">
            <Grid container spacing={1} alignItems="center">
              <Grid item>
                <Logo sx={{ display: 'flex', fontSize: 52, width: 'auto' }} />
              </Grid>
            </Grid>
          </StyledLink>

          <Link
            to="/favorite"
            style={{ textDecoration: 'none', color: 'white', display: 'flex', alignItems: 'center', gap: 8 }}
          >
            <DiamondDice sx={{ color: 'white' }} />
            <Typography variant="h5" fontFamily="Amatic SC" fontWeight={700}>
              Favorite Spells
            </Typography>
          </Link>
        </Toolbar>
      </Container>
    </MuiAppBar>
  );
};

export default AppBar;
