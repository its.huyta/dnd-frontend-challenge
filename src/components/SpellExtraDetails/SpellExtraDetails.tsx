import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

import { Spell } from '@/types';

import Tag from '../Tag';

interface SpellExtraDetailsProps {
  spell: Spell;
}

const SpellExtraDetails: React.FC<SpellExtraDetailsProps> = ({ spell }) => {
  return (
    <Grid container flexDirection="column" gap={1}>
      {spell?.descriptions.map((description) => (
        <Grid item key={description}>
          <Typography variant="body2">{description}</Typography>
        </Grid>
      ))}
      <Grid container alignItems="center" gap={1}>
        <Grid item>
          <Typography variant="overline">Classes:</Typography>
        </Grid>
        {spell?.classes?.map(({ name }) => (
          <Grid item key={name}>
            <Tag label={name} />
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default SpellExtraDetails;
