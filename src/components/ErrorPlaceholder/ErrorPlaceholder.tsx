import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import Logo from '@/icons/Logo';

const ErrorPlaceholder = () => {
  return (
    <Box position="relative" display="flex" flexDirection="column" width="fit-content" alignItems="center">
      <Logo sx={{ ml: { xs: 4, md: 8 }, width: { xs: 240, md: 320 }, height: 'auto', color: '#eb0029' }} />
      <Typography variant="h4" color="text.primary" fontFamily="Amatic SC" fontWeight={700} textAlign="center">
        Something went wrong
      </Typography>
      <Typography variant="subtitle1" color="text.primary" textAlign="center">
        We&apos;re sorry that you&apos;re encountering this. Please try again later.
      </Typography>
    </Box>
  );
};

export default ErrorPlaceholder;
