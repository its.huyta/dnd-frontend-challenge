import MuiChip from '@mui/material/Chip';
import { styled } from '@mui/material/styles';
import { memo } from 'react';

const Chip = styled(MuiChip)(({ theme }) => ({
  background: 'transparent',
  fontSize: '0.625rem',
  textTransform: 'uppercase',
  fontWeight: 700,
  border: `1px solid ${theme.palette.grey[400]}`,
}));

interface TagProps {
  label: string;
}

const Tag: React.FC<TagProps> = ({ label }) => {
  return <Chip label={label} size="small" />;
};

export default memo(Tag);
