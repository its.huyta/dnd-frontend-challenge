import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import FilterRing from '@/icons/FilterRing';
import { getClassIconUrl } from '@/services/classes';
import { allClasses } from '@/services/spells';
import { ClassIndex } from '@/types';

interface SpellFiltersProps {
  currentClassFilter?: ClassIndex | string;
  onFilterClass?: (classFilter: ClassIndex | string) => void;
}

const SpellFilters: React.FC<SpellFiltersProps> = ({ currentClassFilter, onFilterClass }) => {
  const changeClassFilter = (classIndex: ClassIndex | string) => () => {
    if (!onFilterClass) return;

    onFilterClass(classIndex === currentClassFilter ? '' : classIndex);
  };

  return (
    <Box
      display="flex"
      justifyContent="center"
      flexWrap="wrap"
      margin={{ xs: 2, md: 4 }}
      marginBottom={{ xs: 4, md: 4 }}
      columnGap={{ xs: 2, md: 4, lg: 6 }}
      rowGap={{ xs: 3, md: 4 }}
    >
      {allClasses.map((classIndex) => {
        const isSelected = currentClassFilter === classIndex;

        return (
          <Box
            key={classIndex}
            onClick={changeClassFilter(classIndex)}
            display="flex"
            flexDirection="column"
            alignItems="center"
            textTransform="uppercase"
            sx={{
              cursor: 'pointer',
              opacity: isSelected ? 1 : 0.3,
              '.MuiAvatar-root': {
                marginTop: { xs: isSelected ? 0 : 2, md: isSelected ? -2 : 1 },
              },
              '.MuiAvatar-img': {
                height: { xs: isSelected ? 40 : 32, md: isSelected ? 64 : 56 },
              },
              '&:hover': {
                opacity: 1,
                '.MuiAvatar-root': {
                  marginTop: { xs: 0, md: -2 },
                },
                '.MuiAvatar-img': {
                  height: { xs: 40, md: 64 },
                },
              },
            }}
            position="relative"
          >
            {currentClassFilter === classIndex && (
              <FilterRing
                className="animate__animated animate__fadeIn"
                sx={{
                  color: '#c39031',
                  position: 'absolute',
                  top: { xs: -10, md: -32 },
                  fontSize: { xs: 56, md: 100 },
                }}
              />
            )}
            <Box height={{ xs: 40, md: 64 }} display="flex" alignItems="center" justifyContent="center">
              <Avatar
                src={getClassIconUrl(classIndex)}
                alt={classIndex}
                sx={{
                  width: 'fit-content',
                  height: 'fit-content',
                  transition: 'all 0.3s',
                }}
                imgProps={{
                  sx: {
                    width: 'auto',
                    '&:hover': {
                      xs: 40,
                      md: 64,
                    },
                    transition: 'all 0.3s',
                  },
                }}
              />
            </Box>
            <Typography variant="body2" fontWeight={600} marginTop={2}>
              {classIndex.toUpperCase()}
            </Typography>
          </Box>
        );
      })}
    </Box>
  );
};

export default SpellFilters;
