import Grid from '@mui/material/Grid';
import { memo, useCallback } from 'react';

import { Spell } from '@/types';

import SpellCard from '../SpellCard';

export interface SpellList {
  spells: Spell[];
  isFavoriteSpell: (spell: Spell) => boolean;
  onRemoveFavorite?: (spell: Spell) => void;
  onAddFavorite?: (spell: Spell) => void;
}

const SpellList: React.FC<SpellList> = ({ spells, isFavoriteSpell, onAddFavorite, onRemoveFavorite }) => {
  if (!spells?.length) return null;

  const handleAddFavorite = useCallback(
    (spell: Spell) => {
      if (onAddFavorite) onAddFavorite(spell);
    },
    [onAddFavorite],
  );

  const handleRemoveFavorite = useCallback(
    (spell: Spell) => {
      if (onRemoveFavorite) onRemoveFavorite(spell);
    },
    [onRemoveFavorite],
  );

  return (
    <Grid container gap={2} flexDirection={{ xs: 'row', lg: 'column' }}>
      {spells.map((spell) => (
        <Grid
          item
          key={spell.index}
          xs={12}
          lg={12}
          width="100%"
          maxWidth={(theme) => ({ sm: `calc(50% - ${theme.spacing(1)})`, lg: '100%' })}
          flexGrow={1}
        >
          <SpellCard
            spell={spell}
            isFavorite={isFavoriteSpell(spell)}
            onAddFavorite={handleAddFavorite}
            onRemoveFavorite={handleRemoveFavorite}
          />
        </Grid>
      ))}
    </Grid>
  );
};

export default memo(SpellList);
