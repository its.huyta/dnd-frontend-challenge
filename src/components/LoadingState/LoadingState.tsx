import Box from '@mui/material/Box';

import LoadingSpinner from '@/icons/LoadingSpinner';

const LoadingState = () => {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      position="fixed"
      top={0}
      right={0}
      bottom={0}
      left={0}
      zIndex={10}
    >
      <Box
        position="fixed"
        top={0}
        right={0}
        bottom={0}
        left={0}
        zIndex={9}
        bgcolor="common.black"
        sx={{ opacity: 0.3 }}
      />
      <LoadingSpinner sx={{ fontSize: { xs: 200, sm: 400 } }} />
    </Box>
  );
};

export default LoadingState;
