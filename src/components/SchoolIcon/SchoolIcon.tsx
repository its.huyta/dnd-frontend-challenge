import Avatar from '@mui/material/Avatar';
import Tooltip from '@mui/material/Tooltip';
import { memo } from 'react';

import { getSchoolIconUrl } from '@/services/schools';
import { School } from '@/types';

interface SchoolIconProps {
  school: School;
}

const SchoolIcon: React.FC<SchoolIconProps> = ({ school }) => {
  return (
    <Tooltip title={school}>
      <Avatar
        src={getSchoolIconUrl(school)}
        alt={school}
        variant="rounded"
        sx={{ width: 32, height: 32, borderRadius: '50%' }}
      />
    </Tooltip>
  );
};

export default memo(SchoolIcon);
