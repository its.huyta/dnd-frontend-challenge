import Box from '@mui/material/Box';

import Logo from '@/icons/Logo';

const LogoWatermark = () => {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      top={0}
      bottom={0}
      left={0}
      right={0}
      position="fixed"
      zIndex={-1}
    >
      <Logo sx={{ fontSize: 320, opacity: 0.05 }} />
    </Box>
  );
};

export default LogoWatermark;
