import Avatar from '@mui/material/Avatar';
import { memo } from 'react';

import { getFallbackSpellIconUrl, getSpellIconUrl } from '@/services/spells';
import { Spell } from '@/types';

interface SpellIconProps {
  spell: Spell;
}

console.log('Avatar', Avatar);

const SpellIcon: React.FC<SpellIconProps> = ({ spell }) => {
  const renderIcon = (spellIconUrl: string, children?: React.ReactNode) => {
    return (
      <Avatar
        src={spellIconUrl}
        alt={spell.school}
        data-testid={spell.school}
        sx={{ width: 32, height: 32, bgcolor: '#1a202c' }}
        variant="circular"
      >
        {children}
      </Avatar>
    );
  };

  return <>{renderIcon(getSpellIconUrl(spell.name), renderIcon(getFallbackSpellIconUrl()))}</>;
};

export default memo(SpellIcon);
