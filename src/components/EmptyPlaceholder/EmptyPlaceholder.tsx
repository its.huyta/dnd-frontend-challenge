import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import Logo from '@/icons/Logo';

interface EmptyPlaceholderProps {
  subtitleText?: string;
}

const EmptyPlaceholder: React.FC<EmptyPlaceholderProps> = ({ subtitleText = 'Please try with another filter.' }) => {
  return (
    <Box position="relative" display="flex" flexDirection="column" width="fit-content" alignItems="center">
      <Logo sx={{ ml: { xs: 4, md: 8 }, width: { xs: 240, md: 320 }, height: 'auto' }} color="primary" />
      <Typography variant="h4" color="text.primary" fontFamily="Amatic SC" fontWeight={700} textAlign="center">
        No data to display
      </Typography>
      <Typography variant="subtitle1" color="text.primary" textAlign="center">
        {subtitleText}
      </Typography>
    </Box>
  );
};

export default EmptyPlaceholder;
