import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import IconButton from '@mui/material/IconButton';
import { memo, useCallback } from 'react';

import { Spell } from '@/types/spell';

export interface ToggleFavoriteButtonProps {
  spell: Spell;
  isFavorite?: boolean;
  onAddFavorite?: (spell: Spell) => void;
  onRemoveFavorite?: (spell: Spell) => void;
}

const ToggleFavoriteButton = ({ spell, isFavorite, onAddFavorite, onRemoveFavorite }: ToggleFavoriteButtonProps) => {
  const toggleFavorite = useCallback(
    (event: React.MouseEvent) => {
      event.stopPropagation();

      if (isFavorite) return onRemoveFavorite?.(spell);

      onAddFavorite?.(spell);
    },
    [isFavorite, onAddFavorite, onRemoveFavorite],
  );

  return (
    <IconButton color="inherit" onClick={toggleFavorite} data-testid={`spell-favorite-button-${spell.index}`}>
      {isFavorite ? <FavoriteIcon color="error" /> : <FavoriteBorderIcon />}
    </IconButton>
  );
};

export default memo(ToggleFavoriteButton);
