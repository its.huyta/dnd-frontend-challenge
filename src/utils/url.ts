export const joinURL = (baseUrl: string, url: string) => {
  return new URL(url, baseUrl).href;
};
