import { useQuery } from '@apollo/client';
import { useEffect, useState } from 'react';

import { DEFAULT_LIMIT, DEFAULT_OFFSET } from '@/config';
import { FETCH_SPELLS_QUERY, fetchSpellCount, FetchSpellsResponse, normalizeSpell } from '@/persistence/api/spells';
import { ClassIndex } from '@/types';

interface UseSpellsProps {
  offset?: number;
  limit?: number;
  classIndex?: ClassIndex | string;
}

export const useSpells = ({ offset = DEFAULT_OFFSET, limit = DEFAULT_LIMIT, classIndex }: UseSpellsProps) => {
  const [count, setCount] = useState<number | null>(null);
  const { loading, error, data } = useQuery<FetchSpellsResponse>(FETCH_SPELLS_QUERY, {
    variables: {
      offset,
      limit,
      ...(classIndex ? { classIndex } : {}),
    },
  });

  useEffect(() => {
    const startFetching = async () => {
      setCount(null);

      const spellCount = await fetchSpellCount(classIndex);

      setCount(spellCount);
    };

    startFetching();
  }, [classIndex]);

  return {
    count,
    loading: loading,
    error: error,
    data: data?.spells.map(normalizeSpell),
  };
};
