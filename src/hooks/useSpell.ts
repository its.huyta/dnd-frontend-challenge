import { useQuery } from '@apollo/client';

import { FETCH_SPELL_QUERY, normalizeSpell } from '@/persistence/api/spells';
import { RawSpell } from '@/types';

export const useSpell = async (index: string) => {
  const { loading, error, data } = useQuery<RawSpell>(FETCH_SPELL_QUERY, {
    variables: {
      index,
    },
  });

  return { loading, error, data: data ? normalizeSpell(data) : undefined };
};
