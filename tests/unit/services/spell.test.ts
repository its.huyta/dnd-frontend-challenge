import { paramCase } from 'change-case';

import { SPELL_ICON_ORIGIN_URL } from '../../../src/config';
import { getFallbackSpellIconUrl, getSpellIconUrl } from '../../../src/services/spells';
import * as urlUtils from '../../../src/utils/url';

describe('SpellService', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('getSpellIconUrl method', () => {
    test('should modify and return the joined image name if the spell name is an exception', async () => {
      const joinUrlMock = jest.spyOn(urlUtils, 'joinURL');
      const spellIconUrl = getSpellIconUrl('Animate Dead');

      expect(joinUrlMock).toHaveBeenCalledTimes(1);
      expect(joinUrlMock).toHaveBeenCalledWith(SPELL_ICON_ORIGIN_URL, 'necromancy.png');
      expect(spellIconUrl).toBe(`${SPELL_ICON_ORIGIN_URL}necromancy.png`);
    });

    test('should return the joined image name if the spell name is not an exception', async () => {
      const joinUrlMock = jest.spyOn(urlUtils, 'joinURL');
      const regularSpellName = 'A regular spell';
      const kebabCasedRegularSpellName = paramCase(regularSpellName);
      const spellIconUrl = getSpellIconUrl(regularSpellName);

      expect(joinUrlMock).toHaveBeenCalledTimes(1);
      expect(joinUrlMock).toHaveBeenCalledWith(SPELL_ICON_ORIGIN_URL, `${kebabCasedRegularSpellName}.png`);
      expect(spellIconUrl).toBe(`${SPELL_ICON_ORIGIN_URL}${kebabCasedRegularSpellName}.png`);
    });
  });

  describe('getFallbackSpellIconUrl', () => {
    test('should return the fallback image url', async () => {
      const joinUrlMock = jest.spyOn(urlUtils, 'joinURL');
      const fallbackImageName = 'conjuration.png';
      const fallbackSpellIconUrl = getFallbackSpellIconUrl();

      expect(joinUrlMock).toHaveBeenCalledTimes(1);
      expect(joinUrlMock).toHaveBeenCalledWith(SPELL_ICON_ORIGIN_URL, fallbackImageName);
      expect(fallbackSpellIconUrl).toBe(`${SPELL_ICON_ORIGIN_URL}conjuration.png`);
    });
  });
});
