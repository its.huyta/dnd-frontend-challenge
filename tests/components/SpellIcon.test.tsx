import { render, screen } from '@testing-library/react';
import React from 'react';

import SpellIcon from '../../src/components/SpellIcon';
import { ClassIndex, School } from '../../src/types';

jest.mock(
  '@mui/material/Avatar',
  () => (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>) => {
    return <div {...props} />;
  },
);

describe('SpellIcon component', () => {
  test('should render a spell icon with a fallback', async () => {
    const spellMock = {
      index: 'alarm',
      level: 1,
      name: 'Alarm',
      descriptions: ['You set an alarm against unwanted intrusion'],
      castingTime: '1 minute',
      duration: '8 hours',
      attackType: null,
      range: '30 feet',
      school: School.ABJURATION,
      classes: [ClassIndex.RANGER],
    };

    render(<SpellIcon spell={spellMock} />);

    const spellIcon = screen.getAllByTestId(spellMock.school)[0];

    expect(spellIcon).toBeTruthy();
    expect(spellIcon.children.length).toBe(1);
    expect(spellIcon.children[0].getAttribute('data-testid')).toBe(spellMock.school);
  });
});
