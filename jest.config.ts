import { pathsToModuleNameMapper } from 'ts-jest';

module.exports = {
  moduleNameMapper: {
    '@/(.*)': '<rootDir>/src/$1',
  },
  testEnvironment: 'jsdom',
};
