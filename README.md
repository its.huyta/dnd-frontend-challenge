## DnD Frontend Challenge

This is an application to list spells from Dungeons & Dragons.

#### Tech Stack
- [ReactJS](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [React Router](https://reactrouter.com/en/main)
- [MUI (CSS in JS)](https://mui.com/)
- [Jest (testing framework)](https://jestjs.io/)
- [Apollo Client](https://www.apollographql.com/docs/react/)

#### Development Instructions

1. Install the dependencies:

```bash
yarn
```

2. Spin things up:

```bash
yarn dev
```

Open [http://localhost:5173](http://localhost:5173) in your browser and happy coding!

#### Testing

1. Run unit tests:

```bash
yarn test
```

2. E2E tests can be referred on [this repository](https://gitlab.com/its.huyta/dnd-frontend-challenge-e2e-tests).

#### Live Demo

The live demo is available at https://dnd-spell-list.netlify.app/.
